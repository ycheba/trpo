﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.IO;

namespace aga_PAZDA
{
    public class AppContext: DbContext
    {
        public AppContext()
        : base($"Server=(localdb)\\MSSQLLocalDB;Database=MyDebil;AttachDbFilename={Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).ToString())}\\MyDebil.mdf;Integrated Security=True;")
        { }
        public DbSet<Table.Worker> Workers { get; set; }
    }
}
