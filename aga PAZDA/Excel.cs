﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;

namespace aga_PAZDA
{
    public class Excel
    {
        public void createFile(string str, int column)
        {
            List<Table.Worker> list = new DB().Find(str, column);
            createFile(list);
        }

        public void createFile()
        {
            List<Table.Worker> list = new DB().GetTable();
            createFile(list);
        }

        public void createFile(List<Table.Worker> list)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var path = @"" + fbd.SelectedPath + "\\Сотрудники.xlsx";
                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

                using (ExcelPackage pck = new ExcelPackage())
                {
                    var table = pck.Workbook.Worksheets.Add("Persons");
                    table.Cells[2, 1].LoadFromCollection(list, false);
                    table.Column(6).Style.Numberformat.Format = "dd-MM-yyyy";
                    table.Cells["B1"].Value = "ID сотрудника";
                    table.Cells["C1"].Value = "Имя";
                    table.Cells["D1"].Value = "Фамилия";
                    table.Cells["E1"].Value = "Отчество";
                    table.Cells["F1"].Value = "Дата рождения";
                    table.Cells["G1"].Value = "Телефон";
                    table.Cells["H1"].Value = "Отдел";
                    table.DeleteColumn(1);
                    if (list.Count > 0) table.Cells[1, 1, list.Count, 7 + 2].AutoFitColumns();
                    try
                    {
                        pck.SaveAs(new FileInfo(path));
                    }
                    catch (System.InvalidOperationException)
                    {
                        System.Windows.MessageBox.Show("Файл уже открыт. Для его сохранения выберете другой путь или закройте файл в Excel", "Ошибка сохранения", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                }
            }
        }
    }
}
