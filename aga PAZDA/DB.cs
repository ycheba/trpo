﻿using aga_PAZDA.Table;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media.Animation;

namespace aga_PAZDA
{
    public class DB
    {
        public List<Worker> GetTable()
        {
            return new AppContext().Workers.ToList();
        }

        public void ClearAll()
        {
            using (var db = new AppContext())
            {
                var query = from b in db.Workers select b;
                foreach (var item in query)
                {
                    db.Workers.Remove(item);
                }
                db.SaveChanges();
            }
        }

        public void Add(string name, string surname, string patronymic, int ident, DateTime dateOfBirth, string telephone, string department)
        {
            using (var db = new AppContext())
            {
                var worker = new Table.Worker
                {
                    Name = name,
                    Surname = surname,
                    Patronymic = patronymic,
                    Ident = ident,
                    DateOfBirth = dateOfBirth,
                    Telephone = telephone,
                    Department = department
                };
                db.Workers.Add(worker);
                db.SaveChanges();
            }
        }

        public List<Worker> Find(String str, int column = 0)
        {
            Dictionary<int, string> columns = new Dictionary<int, string>()
            {
                [0] = "name",
                [1] = "surname",
                [2] = "patronymic",
                [3] = "ident",
                [4] = "dateOfBirth",
                [5] = "telephone",
                [6] = "department",
            };
            string val = "";
            List<Worker> list = new List<Worker>();
            string request = string.Format($"SELECT * FROM dbo.Workers WHERE {columns[column]} LIKE N'%{str}%'");
            using (var db = new AppContext())
            {
                var query = db.Workers.SqlQuery(request);
                return query.ToList();
            }
        }
    }
}
