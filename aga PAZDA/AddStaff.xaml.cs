﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace aga_PAZDA
{
    /// <summary>
    /// Логика взаимодействия для AddStaff.xaml
    /// </summary>
    public partial class AddStaff : Window
    {
        Button addStaffer;
        public DataGrid table;
        const int countMandatory = 6, countNotMandatory = 1;
        readonly String[] nameMandatryColumn = new String[countMandatory] { "Имя", "Фамилия", "ID", "Дата Рождения", "Телефон", "Отдел" };
        public AddStaff(Button addStaff, DataGrid table)
        {
            InitializeComponent();
            addStaffer = addStaff;
            this.table = table;
            Closing += AddStuffer_Closing;
        }

        private void AddStuffer_Closing(object sender, CancelEventArgs e)
        {
            addStaffer.IsEnabled = true;
            //table.Content = new WorkerTable();
        }

        private void Artur_Click(object sender, RoutedEventArgs e)
        {
            String[] mandatoryInfo = new String[countMandatory] { Name.Text, Surname.Text, Id.Text, Birthday.Text, Telephone.Text, Department.Text.ToLower() };
            String[] notMandatoryInfo = new String[countNotMandatory] { Patronymic.Text };
            if (!firstValidations(mandatoryInfo))
                return;
            int id;
            if (!(int.TryParse(mandatoryInfo[2], out id)))
            {
                displayError("Значение \"ID\" должно быть целочисленным значением");
                return;
            }
            mandatoryInfo[3] = mandatoryInfo[3].Replace('.', '-');
            DateTime date;
            if (!(DateTime.TryParse(mandatoryInfo[3], out date)))
            {
                displayError("\"День рождения\" указан не верно. Данное поле заполняется по шаблону День.Месяц.Год или День-Месяц-Год");
                return;
            }
            if ((int.TryParse(mandatoryInfo[4].Replace('+', ' '), out int value))) // && ((mandatoryInfo[4].Length == 11 && mandatoryInfo[4][0] == '8') || mandatoryInfo[4].Length == 12)
            {
                displayError("Значение \"Телефон\" указано не верно. Номер должен начинаться с +7 или 8");
                return;
            }
            new DB().Add(mandatoryInfo[0], mandatoryInfo[1], notMandatoryInfo[0], id, date, mandatoryInfo[4], mandatoryInfo[5]);
            MessageBox.Show("Работник добавлен в систему", "Добавлен", MessageBoxButton.OK, MessageBoxImage.Information);
            table.ItemsSource = new DB().GetTable();
            Close();
        }

        private bool firstValidations(String[] mandatoryInfo)
        {
            for (int i = 0; i < countMandatory; i++)
                if (mandatoryInfo[i] == "")
                {
                    displayError("Значение в поле \"" + nameMandatryColumn[i] + "\" должно быть заполнено");
                    return false;
                }
            return true;
        }

        private void displayError(String ex)
        {
            MessageBox.Show(ex, "Ошибка заполнения полей", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
