﻿using aga_PAZDA.Table;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aga_PAZDA
{
    public class Jsoner
    {
        public void SaveJson(string str, int len, int column)
        {
            List<Worker> list = new DB().Find(str, column);
            SaveJson(list);
        }
        public void SaveJson()
        {
            List<Worker> list = new DB().GetTable();
            SaveJson(list);
        }

        public void SaveJson(List<Worker> list)
        {
            JsonSerializerOptions options = new JsonSerializerOptions()
            {
                WriteIndented = true,
                Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping
            };
            string json = JsonSerializer.Serialize(list, options);
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var path = @"" + fbd.SelectedPath + "\\staff.json";
                File.WriteAllText(path, json);
            }
        }
    }
}
