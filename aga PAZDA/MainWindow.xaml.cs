﻿using aga_PAZDA.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace aga_PAZDA
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            workerGrid1.ItemsSource = new DB().GetTable();
            
        }

        private void AddStuff(object sender, RoutedEventArgs e)
        {
            AddStaff.IsEnabled = false;
            AddStaff AddStuff = new AddStaff(AddStaff, workerGrid1);
            AddStuff.Show();
        }

        private void ExportJSON(object sender, RoutedEventArgs e)
        {
            new Jsoner().SaveJson(Search());
        }

        private void ExportExcel(object sender, RoutedEventArgs e)
        {
            new Excel().createFile(Search());
        }

        private void Change(object sender, TextChangedEventArgs e)
        {
            workerGrid1.ItemsSource = Search();
        }

        private void SelectColumn(object sender, SelectionChangedEventArgs e)
        {
            workerGrid1.ItemsSource = Search();
        }

        private List<Worker> Search()
        {
            if (Text.Text.Length == 0)
            {
                return new DB().GetTable();
            }
            else
            {
                return new DB().Find(Text.Text.ToString().ToLower(), Column.SelectedIndex);
            }
        }
    }
}
